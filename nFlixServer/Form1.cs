﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace nFlixServer
{
    public partial class Form1 : Form
    {
        [DllImport("user32.dll", CharSet = CharSet.Auto, CallingConvention = CallingConvention.StdCall)]
        private static extern bool SystemParametersInfo(int uAction, int uParam, ref int lpvParam, int flags);

        [DllImport("user32.dll", CharSet = CharSet.Auto, CallingConvention = CallingConvention.StdCall)]
        public static extern void mouse_event(uint dwFlags, uint dx, uint dy, uint cButtons, uint dwExtraInfo);

        [DllImport("kernel32.dll", CharSet = CharSet.Auto, SetLastError = true)]
        public static extern uint SetThreadExecutionState(uint state);

        private const int _CLICKDOWN = 0x02;
        private const int _CLICKUP = 0x04;

        const int screenSaverActiveFlag = 17;
        const int screenSaverUpdateProfile = 2;
        int screenSaverRetVal = 0;
        private int[] lastCoords = { 0, 0 };
        System.Windows.Forms.Timer preventDispSleep;
        private TcpListener listener;
        private Thread networkThread;
        private Thread cThread;
        private bool progRunning = true;
        
        public Form1()
        {
            InitializeComponent();
            notificationIcon.Visible = false;
            this.Resize += ImportStatusForm_Resize;
            preventDispSleep = new System.Windows.Forms.Timer();
            preventDispSleep.Interval = 60000;
            preventDispSleep.Tick += new EventHandler(Timer_Tick);
           
            setIpInfo();

            listener = new TcpListener(IPAddress.Any, 6969);
            networkThread = new Thread(new ThreadStart(Listen));
            networkThread.IsBackground = true;
            networkThread.Start();


        }

        private void Listen()
        {
            listener.Start();

            while (progRunning)
            {
                TcpClient mClient = listener.AcceptTcpClient();
                cThread = new Thread(new ParameterizedThreadStart(HandleClient));
                cThread.IsBackground = true;
                cThread.Start(mClient);
            }
        }

        private void HandleClient(object client)
        {
            TcpClient mClient = (TcpClient)client;
            NetworkStream cStream = mClient.GetStream();

            byte[] cmd = new byte[4096];
            int readBytes;


            while (progRunning)
            {
                readBytes = 0;

                try
                {
                    readBytes = cStream.Read(cmd, 0,4096);
                    this.Invoke((MethodInvoker)delegate
                    {
                        connectStatus.Text = "Connection Status: Connected"; 
                    });
                }
                catch (Exception exp)
                {
                    MessageBox.Show(exp.Message.ToString());
                }

                if (readBytes == 0)
                {
                    this.Invoke((MethodInvoker)delegate
                    {
                        connectStatus.Text = "Connection Status: Disconnected";
                    });
                    break;
                    
                }
                ASCIIEncoding encoder = new ASCIIEncoding();
                bool performed = performRemoteCommand(encoder.GetString(cmd,0,readBytes));

                if (performed)
                {
                    string response = "ok";
                    byte[] rspnse = System.Text.Encoding.ASCII.GetBytes(response);
                    cStream.Write(rspnse, 0, rspnse.Length);
                    //cStream.Flush();
                }
            }
            
        }

        private void ImportStatusForm_Resize(object sender, EventArgs e)
        {
            if (FormWindowState.Minimized == this.WindowState)
            {
                notificationIcon.Visible = true;
                this.ShowInTaskbar = false;
            }
            
        }

        private void notificationIcon_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            this.WindowState = FormWindowState.Normal;
            this.ShowInTaskbar = true;
            notificationIcon.Visible = false;
        }

        private void setIpInfo()
        {
            IPHostEntry localIPs = Dns.GetHostByName(Dns.GetHostName());
            foreach (IPAddress ipaddress in localIPs.AddressList)
            {
                if (ipaddress.AddressFamily == AddressFamily.InterNetwork)
                {
                    ipInfo.Text = "Local IP Address: " + ipaddress.ToString();
                    break;
                }
            }
        }

        private void Timer_Tick(object sender, EventArgs e)
        {
            SetThreadExecutionState(0x00000002);
        }

        private bool performRemoteCommand(string command)
        {
            bool retVal = false;
            switch (command)
            {
                case "PAUSE":
                    SendKeys.SendWait(" ");
                    retVal = true;
                    break;
                case "VOLDOWN":
                    SendKeys.SendWait("{DOWN}");
                    retVal = true;
                    break;
                case "VOLUP":
                    SendKeys.SendWait("{UP}");
                    retVal = true;
                    break;
                case "FORWARD":
                    SendKeys.SendWait("{RIGHT}");
                    retVal = true;
                    break;
                case "BACKWARD":
                    SendKeys.SendWait("{LEFT}");
                    retVal = true;
                    break;
                case "DISPINFO":
                    SendKeys.SendWait("{HOME}");
                    retVal = true;
                    break;
                case "MOUSECLICK":
                    clickMouse();
                    retVal = true;
                    break;
                default:
                    if (command.Contains("MC["))
                    {
                        command = command.TrimStart('M','C','[');
                        command = command.TrimEnd(']');
                        string[] coords = command.Split(' ');
                        int x = Convert.ToInt32(coords[0]);
                        int y = Convert.ToInt32(coords[1]);
                        moveMouse(x, y);
                    }
                    else
                    if (command.Contains("*["))
                    {
                        command = command.TrimStart('*', '[');
                        command = command.TrimEnd(']');
                        SendKeys.SendWait(command);
                    }

                    retVal = true;
                    break;
            }

            return retVal;
        }

        private int[] getMouseOrigin()
        {
            int[] coords = {MousePosition.X, MousePosition.Y};
            return coords;
        }

        private void moveMouse(int x, int y)
        {
            //if (((Cursor.Position.X + x > lastCoords[0] + 10) || (Cursor.Position.X + x < lastCoords[0] - 10)) && ((Cursor.Position.Y + y > lastCoords[1] + 10) || (Cursor.Position.Y + y < lastCoords[1] - 10)))
            //{
                Cursor.Position = new Point(Cursor.Position.X + x, Cursor.Position.Y + y);
                //lastCoords[0] = Cursor.Position.X + x;
                //lastCoords[1] = Cursor.Position.Y + y;
            //}
        }

        private void clickMouse()
        {
            int[] currPos = getMouseOrigin();
            mouse_event(_CLICKDOWN | _CLICKUP, Convert.ToUInt32(currPos[0]), Convert.ToUInt32(currPos[1]), 0, 0);
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (button1.Text == "Disable Screensaver")
            {
                changeScreenSaver(0);
                SetThreadExecutionState(0x00000002);
                preventDispSleep.Start();
                button1.Text = "Enable Screensaver";
            }
            else
            {
                changeScreenSaver(1);
                preventDispSleep.Stop();
                button1.Text = "Disable Screensaver";
            }
        }

        private void changeScreenSaver(int state)
        {
            SystemParametersInfo(screenSaverActiveFlag, state, ref screenSaverRetVal, screenSaverUpdateProfile);
        }

        protected override void OnFormClosing(FormClosingEventArgs e)
        {
            base.OnFormClosing(e);
            if (preventDispSleep.Enabled)
            {
                preventDispSleep.Stop();
                changeScreenSaver(1);
            }
            
        }

    }
}
